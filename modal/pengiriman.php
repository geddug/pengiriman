<?php $id = $_GET["id"];
include '../inc/koneksi.php';
 ?>
<?php if($id != NULL) { ?>
<?php
$q = "SELECT * from pengiriman where id_pengiriman='$id'";
$sql = mysqli_query($koneksi,$q);
$res = mysqli_fetch_array($sql);
$q = "SELECT * from detail_pengiriman where pengiriman_id='$id'";
$sql = mysqli_query($koneksi,$q);
$det = mysqli_fetch_array($sql);
$q = "SELECT * from detail_barang where pengiriman_id='$id'";
$sqlp = mysqli_query($koneksi,$q);
?>
<input type="hidden" value="1" name="cek">
<input type="hidden" value="<?php echo $id;?>" name="id_pengiriman">

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
		   <label class="col-md-10 control-label">Kota Asal</label>
		   <div class="col-md-10">
			  <input class="form-control" value="<?php echo $res['kota_asal'];?>" name="kota_asal" type="text" required>
		   </div>
		</div>
		<div class="form-group">
		   <label class="col-md-10 control-label">Nama Pengirim</label>
		   <div class="col-md-10">
			  <input class="form-control" value="<?php echo $det['nama_pengirim'];?>" name="nama_pengirim" type="text" required>
		   </div>
		</div>
		<div class="form-group">
		   <label class="col-md-10 control-label">Alamat Pengirim</label>
		   <div class="col-md-10">
			  <input class="form-control" value="<?php echo $det['alamat_pengirim'];?>" name="alamat_pengirim" type="text" required>
		   </div>
		</div>
		<div class="form-group">
		   <label class="col-md-10 control-label">Telp Pengirim</label>
		   <div class="col-md-10">
			  <input class="form-control" value="<?php echo $det['telp_pengirim'];?>" name="telp_pengirim" type="text" required>
		   </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
		   <label class="col-md-10 control-label">Kota Tujuan</label>
		   <div class="col-md-10">
			  <input class="form-control" value="<?php echo $res['kota_tujuan'];?>" name="kota_tujuan" type="text" required>
		   </div>
		</div>
		<div class="form-group">
		   <label class="col-md-10 control-label">Nama Penerima</label>
		   <div class="col-md-10">
			  <input class="form-control" value="<?php echo $det['nama_penerima'];?>" name="nama_penerima" type="text" required>
		   </div>
		</div>
		<div class="form-group">
		   <label class="col-md-10 control-label">Alamat Penerima</label>
		   <div class="col-md-10">
			  <input class="form-control" value="<?php echo $det['alamat_penerima'];?>" name="alamat_penerima" type="text" required>
		   </div>
		</div>
		<div class="form-group">
		   <label class="col-md-10 control-label">Telp Penerima</label>
		   <div class="col-md-10">
			  <input class="form-control" value="<?php echo $det['telp_penerima'];?>" name="telp_penerima" type="text" required>
		   </div>
		</div>
	</div>
	<div class="col-md-12">
		<table class="table">
			<thead>
				<tr>
					<th></th>
					<th>Nama Barang</th>
					<th>Jenis</th>
					<th>Jumlah</th>
					<th>Ukuran</th>
					<th>Berat</th>
					<th>Harga</th>
				</tr>
			</thead>
			<tbody class="tbl_barang">
			<?php $no =0;
			while ($data = mysqli_fetch_array($sqlp)) { ?>
				<tr class="<?php echo $no;?>">
				<td><button class="btn btn-danger" onclick="hapus_barang(<?php echo $no;?>)" type="button">Hapus</button></td>
				<td>
					<input class="form-control" name="nama_barang[]" value="<?php echo $data['nama_barang'];?>" type="text" required>
				</td>
				<td>
			  <select class="form-control" required name="jenis_id[]">
				<option value="" style="display:none;">Pilih</option>
				<?php $q = "SELECT * from jenis_barang";
				$sql = mysqli_query($koneksi,$q);
				while ($dataj = mysqli_fetch_array($sql)) { ?>
				<option <?php if ($dataj['id_jenis'] == $data['jenis_id']) { echo "selected"; } ?> value="<?php echo $dataj['id_jenis'];?>"><?php echo $dataj['jenis'];?></option>
				<?php } ?>
			  </select>
			  </td>
			  <td>
			  <input class="form-control jumlah" name="jumlah[]" value="<?php echo $data['jumlah'];?>" type="text" required>
			  </td>
			  <td>
			  <input class="form-control ukuran" value="<?php echo $data['ukuran'];?>" name="ukuran[]" id="ukuran-<?php echo $no;?>" type="text" required>
			  </td>
			  <td>
			  <input class="form-control berat" value="<?php echo $data['berat'];?>" name="berat[]" id="berat-<?php echo $no;?>" type="text" required>
			  </td>
			  <td>
			  <input class="form-control harga" value="<?php echo $data['harga'];?>" name="harga[]" id="harga-<?php echo $no;?>" type="text" required>
			  </td>
			  </tr>
			<?php $no++; } ?>
			</tbody>
		</table>
		<div class="col-md-10"><button class="btn btn-primary" onclick="tambah_barang()" type="button">Tambah</button></div>
	</div>
</div>
<?php } else { ?>
<input type="hidden" value="0" name="cek">
<input type="hidden" value="" name="id_pengiriman">
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
		   <label class="col-md-10 control-label">Kota Asal</label>
		   <div class="col-md-10">
			  <input class="form-control" name="kota_asal" type="text" required>
		   </div>
		</div>
		<div class="form-group">
		   <label class="col-md-10 control-label">Nama Pengirim</label>
		   <div class="col-md-10">
			  <input class="form-control" name="nama_pengirim" type="text" required>
		   </div>
		</div>
		<div class="form-group">
		   <label class="col-md-10 control-label">Alamat Pengirim</label>
		   <div class="col-md-10">
			  <input class="form-control" name="alamat_pengirim" type="text" required>
		   </div>
		</div>
		<div class="form-group">
		   <label class="col-md-10 control-label">Telp Pengirim</label>
		   <div class="col-md-10">
			  <input class="form-control" name="telp_pengirim" type="text" required>
		   </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
		   <label class="col-md-10 control-label">Kota Tujuan</label>
		   <div class="col-md-10">
			  <input class="form-control" name="kota_tujuan" type="text" required>
		   </div>
		</div>
		<div class="form-group">
		   <label class="col-md-10 control-label">Nama Penerima</label>
		   <div class="col-md-10">
			  <input class="form-control" name="nama_penerima" type="text" required>
		   </div>
		</div>
		<div class="form-group">
		   <label class="col-md-10 control-label">Alamat Penerima</label>
		   <div class="col-md-10">
			  <input class="form-control" name="alamat_penerima" type="text" required>
		   </div>
		</div>
		<div class="form-group">
		   <label class="col-md-10 control-label">Telp Penerima</label>
		   <div class="col-md-10">
			  <input class="form-control" name="telp_penerima" type="text" required>
		   </div>
		</div>
	</div>
	<div class="col-md-12">
		<table class="table">
			<thead>
				<tr>
					<th></th>
					<th>Nama Barang</th>
					<th>Jenis</th>
					<th>Jumlah</th>
					<th>Ukuran</th>
					<th>Berat</th>
					<th>Harga</th>
				</tr>
			</thead>
			<tbody class="tbl_barang">
				<tr class="0">
				<td></td>
				<td>
					<input class="form-control" name="nama_barang[]" type="text" required>
				</td>
				<td>
			  <select class="form-control" required name="jenis_id[]">
				<option value="" style="display:none;">Pilih</option>
				<?php $q = "SELECT * from jenis_barang";
				$sql = mysqli_query($koneksi,$q);
				while ($data = mysqli_fetch_array($sql)) { ?>
				<option value="<?php echo $data['id_jenis'];?>"><?php echo $data['jenis'];?></option>
				<?php } ?>
			  </select>
			  </td>
			  <td>
			  <input class="form-control jumlah" name="jumlah[]" type="text" required>
			  </td>
			  <td>
			  <input class="form-control ukuran" name="ukuran[]" id="ukuran-0" type="text" required>
			  </td>
			  <td>
			  <input class="form-control berat" name="berat[]" id="berat-0" type="text" required>
			  </td>
			  <td>
			  <input class="form-control harga" name="harga[]" id="harga-0" type="text" required>
			  </td>
			  </tr>
			</tbody>
		</table>
		<div class="col-md-10"><button class="btn btn-primary" onclick="tambah_barang()" type="button">Tambah</button></div>
	</div>
</div>
<?php } ?>
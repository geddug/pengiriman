<?php if (empty($_SESSION['id_user'])) {
	echo "<script>location.href='index.php?p=login'</script>";
	die;
} ?>
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">Pengiriman</div>
            <div class="card-body">
               <div align="right">
                  <button onclick="tambah()" class="btn btn-primary">Add</button>
               </div>
               <br />
			   <!-- Nav tabs -->
				<ul class="nav nav-tabs">
				  <li class="nav-item">
					<a class="nav-link active" data-toggle="tab" href="#home">Proses Pengiriman</a>
				  </li>
				  <li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#menu1">Selesai</a>
				  </li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
				  <div class="tab-pane container active" id="home"><br>
					<table class="table table-bordered table-striped datatable">
                  <thead>
                     <tr>
                        <th>No.</th>
                        <th>Resi</th>
						<th>Kota Asal</th>
						<th>Kota Tujuan</th>
						<th>Harga</th>
                        <th width="20%"></th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        $q = "SELECT * from pengiriman where status = 0;";
                        $sql = mysqli_query($koneksi,$q);
                        $no = 1;
                        while ($data = mysqli_fetch_array($sql)) {
                        ?>
                     <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $data['id_pengiriman'];?></td>
						<td><?php echo $data['kota_asal'];?></td>
						<td><?php echo $data['kota_tujuan'];?></td>
						<td><?php echo $data['total_harga'];?></td>
                        <td class="text-center">
                           <a href="index.php?p=surat-pengiriman&id=<?php echo $data['id_pengiriman'];?>" class="btn btn-info" title="print <?php echo $data['id_pengiriman'];?>" style="margin-bottom:10px;">Print</a>
                           &nbsp;
						   <button onclick="status('<?php echo $data['id_pengiriman'];?>')" class="btn btn-warning" title="status <?php echo $data['id_pengiriman'];?>" style="margin-bottom:10px;">Status</button>
                           &nbsp; 
						   <button onclick="edit('<?php echo $data['id_pengiriman'];?>')" class="btn btn-success" title="Edit <?php echo $data['id_pengiriman'];?>" style="margin-bottom:10px;">Edit</button>
                           &nbsp; 
                           <button onclick="hapus('<?php echo $data['id_pengiriman'];?>')" class="btn btn-danger" title="Hapus <?php echo $data['id_pengiriman'];?>" style="margin-bottom:10px;">Hapus</button>
                        </td>
                     </tr>
                     <?php $no++; } ?>
                  </tbody>
               </table>
				  </div>
				  <div class="tab-pane container fade" id="menu1"><br>
					<table class="table table-bordered table-striped datatable">
                  <thead>
                     <tr>
                        <th>No.</th>
                        <th>Resi</th>
						<th>Kota Asal</th>
						<th>Kota Tujuan</th>
						<th>Harga</th>
                        <th width="20%"></th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        $q = "SELECT * from pengiriman where status = 1";
                        $sql = mysqli_query($koneksi,$q);
                        $no = 1;
                        while ($data = mysqli_fetch_array($sql)) {
                        ?>
                     <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $data['id_pengiriman'];?></td>
						<td><?php echo $data['kota_asal'];?></td>
						<td><?php echo $data['kota_tujuan'];?></td>
						<td><?php echo $data['total_harga'];?></td>
                        <td class="text-center">
                           <a href="index.php?p=surat-pengiriman&id=<?php echo $data['id_pengiriman'];?>" class="btn btn-info" title="print <?php echo $data['id_pengiriman'];?>" style="margin-bottom:10px;">Print</a>
						   <button onclick="status('<?php echo $data['id_pengiriman'];?>')" class="btn btn-warning" title="status <?php echo $data['id_pengiriman'];?>" style="margin-bottom:10px;">Status</button>
                           &nbsp; 
						   <button onclick="edit('<?php echo $data['id_pengiriman'];?>')" class="btn btn-success" title="Edit <?php echo $data['id_pengiriman'];?>" style="margin-bottom:10px;">Edit</button>
                           &nbsp; 
                           <button onclick="hapus('<?php echo $data['id_pengiriman'];?>')" class="btn btn-danger" title="Hapus <?php echo $data['id_pengiriman'];?>" style="margin-bottom:10px;">Hapus</button>
                        </td>
                     </tr>
                     <?php $no++; } ?>
                  </tbody>
               </table>
				  </div>
				</div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
function tambah() {
	$('#formp')[0].reset();
	$("#modalp").modal({
    backdrop: 'static',
    keyboard: false
});
	$("#modalbodyp").load("modal/pengiriman.php?id=", function (data) {
		$("#modalbodyp").html(data);
	})
}

function edit(id) {
	$('#formp')[0].reset();
	$("#modalp").modal({
    backdrop: 'static',
    keyboard: false
});
	$("#modalbodyp").load("modal/pengiriman.php?id=" + id, function (data) {
		$("#modalbodyp").html(data);
	})
}

function status(id) {
	$('#form')[0].reset();
	$("#myModal").modal('show');
	$("#modalbody").load("modal/status.php?id=" + id, function (data) {
		$("#modalbody").html(data);
	})
}

function simpanp() {
	$.ajax({
		type: "POST",
		url: "inc/pengiriman.php",
		data: $("#formp").serialize(),
		success: function (data) {
			alert("Berhasil");
			location.reload();
		}
	});
}
function simpan() {
	$.ajax({
		type: "POST",
		url: "inc/status.php",
		data: $("#form").serialize(),
		success: function (data) {
			alert("Berhasil");
			location.reload();
		}
	});
}

function hapus(id) {
	$.ajax({
		url: "inc/pengiriman.php?id=" + id,
		type: "POST",
		//dataType: "JSON",
		success: function (data) {
			alert("Berhasil");
			location.reload();
		},
		error: function (jqXHR, textStatus, errorThrown) {
			alert("Error", "", "error")
		}
	});
}
$(document).on('input',".ukuran", function() {
    var a = $(this).val();
	var id = $(this).attr('id');
	var split_id = id.split("-");
	if (a != '') {
		var b = ukuran(a);
		var c = $("#berat-"+split_id[1]).val();
		if (c == '' || c == 0 || c == null) {
			c = 0;
		} else {
			c = berat(c);
		}
		var d = b+c;
		$("#harga-"+split_id[1]).val(d);
	} else {
		$("#harga-"+split_id[1]).val(0);
	}
});
function ukuran(val) {
	a = parseInt(val);
	var b = 0;
	b = a * 2500;
	return b;
}
function berat(val) {
	a = parseInt(val);
	var b = 0;
	if (a <= 15) {
		b = 50000;
	} else {
		b = a - 15;
		b = b * 2500;
		b = b + 50000;
	}
	return b;
}
$(document).on('input',".berat", function() {
    var a = $(this).val();
	var id = $(this).attr('id');
	var split_id = id.split("-");
	if (a != '') {
		var b = berat(a);
		var c = $("#ukuran-"+split_id[1]).val();
		if (c == '' || c == 0 || c == null) {
			c = 0;
		} else {
			c = ukuran(c);
		}
		var d = b+c;
		$("#harga-"+split_id[1]).val(d);
	} else {
		$("#harga-"+split_id[1]).val(0);
	}
});
var jumlah = 100;
function tambah_barang() {
	$(".tbl_barang").append('<tr class="'+jumlah+'"><td><button class="btn btn-danger" onclick="hapus_barang('+jumlah+')" type="button">Hapus</button></td><td><input class="form-control" name="nama_barang[]" type="text" required></td><td><select class="form-control" required name="jenis_id[]"><option value="" style="display:none;">Pilih</option><?php $q = "SELECT * from jenis_barang";$sql = mysqli_query($koneksi,$q);while ($data = mysqli_fetch_array($sql)) { ?><option value="<?php echo $data['id_jenis'];?>"><?php echo $data['jenis'];?></option><?php } ?></select></td><td><input class="form-control jumlah" name="jumlah[]" type="text" required></td><td><input class="form-control ukuran" id="ukuran-'+jumlah+'" name="ukuran[]" type="text" required></td><td><input class="form-control berat" name="berat[]" id="berat-'+jumlah+'" type="text" required></td><td><input class="form-control harga" id="harga-'+jumlah+'" name="harga[]" type="text" required></td></tr>');
	jumlah = jumlah + 1;
}
function hapus_barang(val) {
	$("."+val).remove();
}
$(".datatable").DataTable({
	dom: 'Bfrtip',
        buttons: [
            'print'
        ]
});
</script>
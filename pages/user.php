<?php if (empty($_SESSION['id_user'])) {
	echo "<script>location.href='index.php?p=login'</script>";
	die;
} ?>
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">User</div>
            <div class="card-body">
               <div align="right">
                  <button onclick="tambah()" class="btn btn-primary">Add</button>
               </div>
               <br />
               <table class="table table-bordered table-striped datatable">
                  <thead>
                     <tr>
                        <th>No.</th>
                        <th>User</th>
                        <th width="20%"></th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        $q = "SELECT * from user";
                        $sql = mysqli_query($koneksi,$q);
                        $no = 1;
                        while ($data = mysqli_fetch_array($sql)) {
                        ?>
                     <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $data['nama'];?></td>
                        <td class="text-center">
                           <button onclick="edit('<?php echo $data['id_user'];?>')" class="btn btn-success" title="Edit <?php echo $data['nama'];?>">Edit</button>
                           &nbsp; 
                           <button onclick="hapus('<?php echo $data['id_user'];?>')"   class="btn btn-danger" title="Hapus <?php echo $data['nama'];?>">Hapus</button>
                        </td>
                     </tr>
                     <?php $no++; } ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
function tambah() {
	$('#form')[0].reset();
	$("#myModal").modal('show');
	$("#modalbody").load("modal/user.php?id=", function (data) {
		$("#modalbody").html(data);
	})
}

function edit(id) {
	$('#form')[0].reset();
	$("#myModal").modal('show');
	$("#modalbody").load("modal/user.php?id=" + id, function (data) {
		$("#modalbody").html(data);
	})
}

function simpan() {
	$.ajax({
		type: "POST",
		url: "inc/user.php",
		data: $("#form").serialize(),
		success: function (data) {
			alert("Berhasil");
			location.reload();
		}
	});
}

function hapus(id) {
	$.ajax({
		url: "inc/user.php?id=" + id,
		type: "POST",
		//dataType: "JSON",
		success: function (data) {
			alert("Berhasil");
			location.reload();
		},
		error: function (jqXHR, textStatus, errorThrown) {
			alert("Error", "", "error")
		}
	});
}
$(".datatable").DataTable();
</script>
<?php if ($_GET != NULL) { 
$id = $_GET["id"];
$q = "SELECT * from pengiriman where id_pengiriman='$id'";
$sql = mysqli_query($koneksi,$q);
$res = mysqli_fetch_array($sql);
$q = "SELECT * from detail_pengiriman where pengiriman_id='$id'";
$sql = mysqli_query($koneksi,$q);
$det = mysqli_fetch_array($sql);
$q = "SELECT * from detail_barang,jenis_barang where detail_barang.jenis_id = jenis_barang.id_jenis and detail_barang.pengiriman_id='$id'";
$sqlp = mysqli_query($koneksi,$q);
?>
<div id="invoice">
    <div class="toolbar hidden-print">
        <div class="text-right">
            <button id="printInvoice" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
        </div>
        <hr>
    </div>
    <div class="invoice overflow-auto">
        <div style="min-width: 600px">
            <header>
                <div class="row">
                    <div class="col">
						<img src="assets/img/logo.png" data-holder-rendered="true" />
                    </div>
                    <div class="col company-details">
                        <div>Tgl Input : <?php echo $res['tgl'];?></div>
                        <div>Tgl Cetak : <?php echo date("Y-m-d H:i:s");?> </div>
                    </div>
                </div>
            </header>
            <main>
                <div class="row contacts">
                    <div class="col invoice-to">
                        <div class="text-gray-light">Pengirim :</div>
                        <h2 class="to"><?php echo $det['nama_pengirim'];?></h2>
                        <div class="address"><?php echo $det['alamat_pengirim'];?></div>
                        <div class="email"><?php echo $det['telp_pengirim'];?></div>
                    </div>
                    <div class="col company-details">
						<div class="text-gray-light">Penerima :</div>
                        <h2 class="name"><?php echo $det['nama_penerima'];?></h2>
                        <div><?php echo $det['alamat_penerima'];?></div>
                        <div><?php echo $det['telp_penerima'];?></div>
                    </div>
                </div>
                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th>Banyak</th>
                            <th class="text-left">Jenis</th>
                            <th class="text-left">Nama Barang</th>
                            <th class="text-right">Berat</th>
                            <th class="text-right">Jumlah Harga</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php while ($data = mysqli_fetch_array($sqlp)) { ?>
                        <tr>
                            <td class="qty"><?php echo $data['jumlah'];?></td>
                            <td class="text-left"><?php echo $data['jenis'];?></td>
                            <td class="text-left"><?php echo $data['nama_barang'];?></td>
                            <td class="qty"><?php echo $data['berat'];?></td>
                            <td class="total"><?php echo number_format($data['harga']);?></td>
                        </tr>
					<?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">TOTAL</td>
                            <td><?php echo number_format($res['total_harga']);?></td>
                        </tr>
                    </tfoot>
                </table>
            </main>
            <footer>
                Invoice was created on a computer and is valid without the signature and seal.
            </footer>
        </div>
        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
        <div></div>
    </div>
</div>
<script>
 $('#printInvoice').click(function(){
	 Popup($('.invoice')[0].outerHTML);
	 function Popup(data) {
		 window.print();
		 return true;
	}
});
</script>
<?php } ?>
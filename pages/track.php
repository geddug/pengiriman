<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Lacak Paket</div>
                <div class="card-body">
                    <form onsubmit="cek();return false;" id="form-login">
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Nomor Resi</label>
                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="id" value="" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Lacak
                                </button>
                            </div>
                        </div>
                    </form>
					<div id="hasil" style="margin-top:20px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function cek() {
	$.ajax({
		type: "POST",
		url: "modal/track.php",
		data: $("#form-login").serialize(),
		success: function(data) {
			$("#hasil").html(data);
		}
	});
}
</script>
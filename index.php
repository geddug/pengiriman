<?php
   session_start();
   include 'inc/koneksi.php';
   if (!empty($_GET['p'])) {
     $p = $_GET['p'];
   } ?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Aplikasi</title>
      <link rel="shortcut icon" href="/images/logo.png">
      <!-- Scripts -->
      <script src="assets/js/jquery.js"></script>
      <script src="assets/js/app.js" defer></script>
      <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	  <script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
	  <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>
      <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
	  <link href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
      <!-- Fonts -->
      <link rel="dns-prefetch" href="//fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
      <!-- Styles -->
      <link href="assets/css/style.css" rel="stylesheet">
   </head>
   <body>
      <div id="app">
         <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
               <img class="img-fluid" src="assets/img/logo.png" width="100" style="margin-right: 10px;">
               <a class="navbar-brand" href="">Aplikasi</a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
               <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <!-- Left Side Of Navbar -->
                  <ul class="navbar-nav mr-auto">
                  </ul>
                  <!-- Right Side Of Navbar -->
                  <ul class="navbar-nav ml-auto">
					<?php if (empty($_SESSION['id_user'])) { ?>
                     <li class="nav-item">
                        <a class="nav-link" href="index.php?p=login">Login</a>
                     </li>
					<?php } else { ?>
					 <li class="nav-item">
                        <a class="nav-link" href="index.php?p=pengiriman">Pengiriman</a>
                     </li>
					 <li class="nav-item">
                        <a class="nav-link" href="index.php?p=user">User</a>
                     </li>
                     <li class="nav-item dropdown">
					 <?php $q = "SELECT * from user WHERE id_user='".$_SESSION['id_user']."'"; 
					 $sql = mysqli_query($koneksi,$q);
					 $res = mysqli_fetch_array($sql); ?>
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre><?php echo $res['nama'];?> <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                           <a class="dropdown-item" href="logout.php">Logout
                           </a>
                        </div>
                     </li>
					<?php } ?>
                  </ul>
               </div>
            </div>
         </nav>
         <main class="py-4">
            <?php //baca dir halaman
               $pages_dir = 'pages';
               if(!empty($p)){
               	$pages = scandir($pages_dir, 0);
               	unset($pages[0], $pages[1]);
               	
               	if(in_array($p.'.php', $pages)){
               		include($pages_dir.'/'.$p.'.php');
               	} else {
               		echo 'Halaman tidak ditemukan! :(';
               	}
               } else {
               	if (!empty($_SESSION['id_user'])) {
               		include($pages_dir.'/home.php');
               	} else {
               		include($pages_dir.'/track.php');
               	}
               }
               ?>
         </main>
      </div>
      <!-- The Modal -->
      <div class="modal" id="myModal">
         <div class="modal-dialog">
            <div class="modal-content">
               <!-- Modal Header -->
               <div class="modal-header">
                  <h4 class="modal-title">Form</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <form id="form" onsubmit="simpan();return false;">
                  <!-- Modal body -->
                  <div class="modal-body" id="modalbody"></div>
                  <!-- Modal footer -->
                  <div class="modal-footer">
                     <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                     <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
	  <!-- The Modal -->
      <div class="modal" id="modalp">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Modal Header -->
               <div class="modal-header">
                  <h4 class="modal-title">Form</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <form id="formp" onsubmit="simpanp();return false;">
                  <!-- Modal body -->
                  <div class="modal-body" id="modalbodyp"></div>
                  <!-- Modal footer -->
                  <div class="modal-footer">
                     <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                     <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </body>
</html>